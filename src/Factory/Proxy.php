<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Factory\Factory;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;

/**
 * Class Proxy.
 *
 * Lazy load of factory object
 *
 * @api
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class Proxy implements FactoryInterface
{
    /**
     * @var ObjectFactoryInterface
     */
    private $objectFactory;

    /**
     * @var string
     */
    private $factoryClass;

    /**
     * @var array
     */
    private $factoryArguments;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * Proxy constructor.
     *
     * @param ObjectFactoryInterface $objectFactory
     * @param string                 $factoryClass
     * @param array                  $factoryArguments
     */
    public function __construct(
        ObjectFactoryInterface $objectFactory,
        string $factoryClass,
        array $factoryArguments = []
    ) {
        $this->objectFactory = $objectFactory;
        $this->factoryClass = $factoryClass;
        $this->factoryArguments = $factoryArguments;
    }

    /**
     * Lazy factory creation.
     *
     * {@inheritdoc}
     */
    public function create(array $arguments = [])
    {
        return $this->getFactory()->create($arguments);
    }

    /**
     * @return FactoryInterface
     */
    private function getFactory(): FactoryInterface
    {
        if (!isset($this->factory)) {
            $this->factory = $this->objectFactory->create(
                $this->factoryClass,
                $this->factoryArguments
            );
        }

        return $this->factory;
    }
}
