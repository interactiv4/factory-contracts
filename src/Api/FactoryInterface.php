<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Factory\Api;

/**
 * Interface FactoryInterface.
 *
 * @api
 */
interface FactoryInterface
{
    /**
     * Create class instance with specified parameters.
     *
     * This interfaces aim to unify factories signature.
     * Factories implementing this interface already know target class name to be instantiated.
     * This method MUST NOT have any mandatory parameter.
     *
     * When target class to be instantiated has mandatory parameters, factory MUST take care of initializing them.
     *
     * Missing strict return type on purpose; if defined, this return type cannot be
     * overridden by implementing classes, even if specified return type is compatible with original one.
     *
     * Allow classes implementing this interface to redefine strict return type value.
     *
     * @param array $arguments
     *
     * @throws \InvalidArgumentException
     *
     * @return object
     */
    public function create(array $arguments = []);
}
