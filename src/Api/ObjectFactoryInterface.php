<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Factory\Api;

/**
 * Interface ObjectFactoryInterface.
 *
 * @api
 */
interface ObjectFactoryInterface
{
    /**
     * Create class instance with specified parameters.
     *
     * This interfaces aim to unify factories signature.
     * Factories implementing this interface receive as parameter target class name to be instantiated.
     * This method MUST only have only mandatory parameter, target class name ($type).
     *
     * When target class to be instantiated has mandatory, no auto-instantiable, parameters,
     * each parameter factory MUST take care of initializing them.
     *
     * Missing strict return type on purpose; if defined, this return type cannot be
     * overridden by extending classes, even if specified return type is compatible with original one.
     *
     * Allow classes implementing this interface to redefine return type value.
     *
     * @param string $type
     * @param array  $arguments
     *
     * @throws \InvalidArgumentException
     *
     * @return object
     */
    public function create(
        string $type,
        array $arguments = []
    );
}
