<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Contracts\Factory\Test\Factory;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Contracts\Factory\Factory\Proxy;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ProxyTest.
 *
 * @internal
 */
class ProxyTest extends TestCase
{
    /**
     * @var ObjectFactoryInterface|MockObject
     */
    private $objectFactory;

    /**
     * @var string
     */
    private $factoryClass = FactoryInterface::class;

    /**
     * @var array
     */
    private $factoryArguments = ['arg1' => 1];

    /**
     * @var Proxy
     */
    private $proxy;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        /* @var ObjectFactoryInterface|MockObject $objectFactory */
        $this->objectFactory = $this->getMockForAbstractClass(ObjectFactoryInterface::class);
        /* @var Proxy proxy */
        $this->proxy = new Proxy(
            $this->objectFactory,
            $this->factoryClass,
            $this->factoryArguments
        );
    }

    /**
     * Test class is an instance of FactoryInterface.
     */
    public function testInstanceOf(): void
    {
        static::assertInstanceOf(FactoryInterface::class, $this->proxy);
    }

    /**
     * Test proxy lazy load.
     */
    public function testProxyLazyLoad(): void
    {
        $createArguments = [
            'createArgument1' => true,
            'createArgument2' => 2,
        ];

        /** @var FactoryInterface|MockObject $factory */
        $factory = $this->getMockForAbstractClass(FactoryInterface::class);

        // Factory creation is expected to be called exactly only once
        $this->objectFactory->expects(static::once())
            ->method('create')
            ->with($this->factoryClass, $this->factoryArguments)
            ->willReturn($factory)
        ;

        // Factory method is expected to be called twice
        $factory->expects(static::exactly(2))
            ->method('create')
            ->with($createArguments)
        ;

        // Call twice to proxy object
        $this->proxy->create($createArguments);
        $this->proxy->create($createArguments);
    }
}
